public class Population {

    private int populationSize;
    private Chromosome[] chromosomes;

    public Population(int populationSize){
        this.populationSize = populationSize;
        this.chromosomes = new Chromosome[populationSize];
    }

    // providing the deep copy of the current chromosomes array
    public Chromosome[] getChromosomes() {
        Chromosome[] cs = new Chromosome[populationSize];

        int index = 0;
        for (Chromosome c : chromosomes){
            cs[index] = c;
        }

        return cs;
    }

    // insert a new chromosome if the chromosomes array is not full
    public void addChromosome(String fileName){
        if (chromosomes.length < populationSize){
            chromosomes[chromosomes.length - 1] = new Chromosome(fileName);
        }
    }

}
