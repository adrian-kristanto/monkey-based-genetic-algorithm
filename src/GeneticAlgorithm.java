public class GeneticAlgorithm {

    private int populationSize = 0;
    private double mutationRate = 0.0;
    private double crossoverRate = 0.0;
    private int generationSize = 0;

    Population p;

    public GeneticAlgorithm(int populationSize, double mutationRate, double crossoverRate, int generationSize){
        this.populationSize = populationSize;
        this.mutationRate = mutationRate;
        this.crossoverRate = crossoverRate;
        this.generationSize = generationSize;
    }

    // 1. Generate initial population
    public Population initPopulation(){
        Population p = new Population(populationSize);
        // call monkey to generate the initial population
        // represent each event sequence with its file name
        return p;
    }
}
